# Dev note

## FastAPI
### [Process field name that have non-alphanumeric characters](https://stackoverflow.com/questions/66185920/pydantic-model-with-field-names-that-have-non-alphanumeric-characters)

### [Handle xml request](https://github.com/tiangolo/fastapi/issues/558)

## ODL api doc
### [Mount new network element](https://docs.opendaylight.org/projects/netconf/en/latest/user-guide.html#spawning-new-netconf-connectors)
* PUT method for rfc8040

## pytest
[Example](https://jerryeml.coderbridge.io/2021/07/11/Create-an-API-with-Flask-and-test-with-pytest/)

## Docker
* cleanup images
```=cmd
# clean images created more then 5 hour
sudo docker image prune -a --filter "until=5h"
```
[Docker official docs](https://docs.docker.com/engine/reference/commandline/image_prune/)
