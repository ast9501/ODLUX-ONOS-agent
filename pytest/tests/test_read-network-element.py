from fastapi.testclient import TestClient

from fake_main import app

import requests
import json

client = TestClient(app)

f = open('pytest/data/read-network-element.json')

payload = json.load(f)

def test_read_network_element():
    url = '/rests/operations/data-provider:read-network-element-connection-list'
    response = client.post(url, json=payload)
    assert response.status_code==200

f.close()
