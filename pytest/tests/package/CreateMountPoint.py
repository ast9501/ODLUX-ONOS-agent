from pydantic import BaseModel, Field
import requests
from requests.auth import HTTPBasicAuth
from typing import TypeVar, Generic, Type, Any
from starlette.requests import Request
from simplexml import loads

T = TypeVar("T", bound=BaseModel)

class NodeBody(BaseModel):
    nodeId: str = Field(alias="node-id")
    host: str
    port: int
    username: str
    password: str
    tcpOnly: bool = Field(alias="tcp-only")
    reconnectOnChangedSchema: bool = Field(alias="reconnect-on-changed-schema")
    connectionTimeout: int = Field(alias="connection-timeout-millis")
    maxConnectionAttempt: int = Field(alias="max-connection-attempts")
    betweenAttemptsTimeout: int = Field(alias="between-attempts-timeout-millis")
    sleepFactor: float = Field(alias="sleep-factor")
    keepaliveDelay: int = Field(alias="keepalive-delay")

class OdlReq(BaseModel):
    node: NodeBody

class XmlBody(Generic[T]):
    def __init__(self, model_class: Type[T]):
        self.model_class = model_class

    async def __call__(self, request: Request) -> T:
        # the following check is unnecessary if always using xml,
        # but enables the use of json too
        if request.headers.get("Content-Type") == "application/xml":
            body = await request.body()
            dict_data = loads(body)
        else:
            dict_data = await request.json()
        return self.model_class.parse_obj(dict_data)