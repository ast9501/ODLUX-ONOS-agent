from fastapi.testclient import TestClient

from fake_main import app
import requests

client = TestClient(app)

def test_get_status():
    url = '/v1/status'
    response = client.get("v1/status")
    assert response.status_code==200
    assert response.json() == {"status": "ok"}