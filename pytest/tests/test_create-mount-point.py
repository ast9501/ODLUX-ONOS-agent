from fastapi.testclient import TestClient

from fake_main import app

import requests
import json

client = TestClient(app)

with open('pytest/data/new-mount-point.xml', 'r') as f:
    payload = f.read()

def test_create_mount_point():
    headers = {'Content-Type': 'application/xml'}
    url = '/rests/data/network-topology:network-topology/topology=topology-netconf/node=new-netconf-device'
    response = client.put(url, data=payload, headers=headers)
    #print(response.content)
    assert response.status_code==201
