from typing import List, Optional
from fastapi import Depends, FastAPI, HTTPException, status, Request, Form
from pydantic import BaseModel, Field

import requests
import secrets

from fastapi.security import HTTPBasic, HTTPBasicCredentials
from requests.auth import HTTPBasicAuth

import os
import json
import time

#import newNetworkElement
from package import NewNetworkElement
from package import CreateMountPoint

app = FastAPI()
security = HTTPBasic()

'''
    Define "Read SDN-R Connection List" request body 
    example:
    {
  "data-provider:input": {
    "filter": [{
      "property": "status",
      "filtervalue": "Connected"
      }
    ],
    "pagination": {
      "size": 10,
      "page": 1
     }
  }
}
'''
class DataProviderInputFilterObj(BaseModel):
    property: str
    filtervalue: str

class DataProviderInputPaginationObj(BaseModel):
    size: int
    page: int

class DataProviderInputObj(BaseModel):
    filter: List[DataProviderInputFilterObj]
    pagination: DataProviderInputPaginationObj

class ConnectListReqItem(BaseModel):
    data_provider_input: DataProviderInputObj = Field(alias="data-provider:input")


'''
    Verify username and password with basic oauth
'''
def verify_user(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, "admin")
    correct_password = secrets.compare_digest(credentials.password, "Kp8bJ4SXszM0WXlhak3eHlcse2gAw84vaoGGmJvUy2U")
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Incorrect username or password",
            headers = {"WWW-Authenticate": "Basic"},
        )
    return credentials.username

'''
    Custom basic network element data struct
'''
class NetworkElement:
    def __init__(self, id, ip, port):
        self.id = id
        self.ip = ip
        self.port = port



'''
API response from ONOS:
API: http://192.168.0.107:8181/onos/v1/network/configuration
response:
{
  "layouts": {},
  "devices": {
    "netconf:192.168.0.107:18300": {
      "basic": {
        "driver": "ovs-netconf"
      },
      "netconf": {
        "ip": "192.168.0.107",
        "port": 18300,
        "username": "netconf",
        "password": "netconf!"
      }
    }
  },
  "ports": {},
  "links": {},
  "hosts": {},
  "regions": {},
  "apps": {
    "org.onosproject.provider.lldp": {
      "suppression": {
        "deviceTypes": [
          "ROADM",
          "OTN",
          "FIBER_SWITCH",
          "OPTICAL_AMPLIFIER",
          "OLS",
          "TERMINAL_DEVICE"
        ],
        "annotation": "{\"no-lldp\":null}"
      }
    }
  }
}
'''

def onos_get_network_elements():
    # Get ONOS ip from env
    url = "http://" + os.environ['ONOS_IP'] + ":8181/onos/v1/network/configuration"
    data = requests.get(url, auth=HTTPBasicAuth('onos', 'rocks')).json()
    devices = data["devices"]

    devicesInfoList = []
    for k, item in devices.items():
        new = NetworkElement(k, item['netconf']['ip'], str(item['netconf']['port']))
        devicesInfoList.append(new)

    return devicesInfoList

def build_odl_read_network_element_response(List):
  response = {}
  providerOutput = {}
  dataList = []
  
  for i in List:
      temp = {}
      '''
      Some property need to figure out how to get from ONOS
      '''
      temp["core-model-capability"] = "Unsupported"
      temp["device-type"] = "O-RAN"
      temp["id"] = i.id
        
      # Encap unavailable capability of network element
      unavailableCap = []
      unavailableCap.append("(urn:sysrepo:plugind?revision=2020-12-10)sysrepo-plugind") # example
        
      # Encap available capability of network element
      availableCap = []
      availableCap.append("(urn:o-ran:dhcp:1.0?revision=2020-12-10)o-ran-dhcp")
        
      nodeDetail = {}
      nodeDetail["unavailable-capabilities"] = unavailableCap
      nodeDetail["available-capabilities"] = availableCap

      temp["node-details"] = nodeDetail
      temp["is-required"] = False
      temp["host"] = i.ip
      temp["port"] = i.port
      temp["node-id"] = i.id
      temp["status"] = "Connected"
        
      dataList.append(temp)
    
  pageDic = {}
  pageDic["size"] = 10
  pageDic["page"] = "1"
  pageDic["total"] = str(len(List))

  providerOutput["data"] = dataList
  providerOutput["pagination"] = pageDic

  response["data-provider:output"] = providerOutput

  #print(json.dumps(response, indent = 4))
  return response

@app.get("/v1/status")
def get_status():
    return {"status": "ok"}

@app.post("/rests/operations/data-provider:read-network-element-connection-list")
def read_network_elements(item: ConnectListReqItem):
#def read_network_elements(item: ConnectListReqItem, credentials: HTTPBasicCredentials = Depends(verify_user)):
  ConnectedDeviceList = onos_get_network_elements()
  response = build_odl_read_network_element_response(ConnectedDeviceList)
  return response

@app.post("/rests/operations/data-provider:create-network-element-connection")
def create_network_elements(item: NewNetworkElement.OdlReq):
#def create_network_elements(item: NewNetworkElement.OdlReq, credentials: HTTPBasicCredentials = Depends(verify_user)):
  #item_dict = item.dict()
  status = NewNetworkElement.SendONOSReq(item.data_provider_input.host, item.data_provider_input.port, item.data_provider_input.username, item.data_provider_input.password, os.environ['ONOS_IP'])
  if (status == 200):
    response = NewNetworkElement.BuildOdlResponse(item.data_provider_input.id, item.data_provider_input.node_id, item.data_provider_input.host, item.data_provider_input.port, item.data_provider_input.username, item.data_provider_input.password, item.data_provider_input.is_required)
    return response

@app.put("/rests/data/network-topology:network-topology/topology=topology-netconf/node={node_name}", status_code=201)
def mount_network_element(item: CreateMountPoint.OdlReq = Depends(CreateMountPoint.XmlBody(CreateMountPoint.OdlReq))):
#def mount_network_element(item: CreateMountPoint.OdlReq = Depends(CreateMountPoint.XmlBody(CreateMountPoint.OdlReq)), credentials: HTTPBasicCredentials = Depends(verify_user)):
  # Reuse api call define in NewNetworkElement to connect to ONOS for creating a new network element on it
  status = NewNetworkElement.SendONOSReq(str(item.node.host), str(item.node.port), item.node.username, item.node.password, os.environ['ONOS_IP'])
  if (status == 200):
    pass

@app.get("/oauth/providers")
def oauth_provider_list():
    f = open('config/oauth-provider.config.json')
    response = json.load(f)
    return response

@app.post("/oauth/login")
def return_oauth_token(username: str = Form(...), password: str = Form(...)):
  if(username == "admin" and password == "admin"):
    access_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBzZG4iLCJyb2xlcyI6WyJ1c2VyIiwiYWRtaW4iXSwiaXNzIjoiT05BUC1TRE5DIiwibmFtZSI6ImFkbWluQHNkbiIsImV4cCI6MTY0NjY1MjY1NywiZmFtaWx5X25hbWUiOiIifQ.9VBxip13i9k-zSg_BqFuhq5SR6_ANlk_hmLnjJLbUGQ"
    expires_at = int(time.time()) + 1000
    token_type = "Bearer"
    return {"access_token": access_token, "expires_at": expires_at, "token_type": token_type}