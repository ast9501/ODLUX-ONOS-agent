from pydantic import BaseModel, Field
import requests
from requests.auth import HTTPBasicAuth

'''
    Define json structure of: Create new NetworkElement connection with SDN-R
    {
      "data-provider:input": {
        "id": "new-mountpoint-name",
        "node-id": "new-mountpoint-name",
        "host": "10.10.10.10",
        "port": "830",
        "username": "netconf",
        "password": "netconf",
        "is-required": "true"
      }
    }
'''
class DataProviderInputObj(BaseModel):
    id: str
    node_id: str = Field(alias="node-id")
    host: str
    port: str
    username: str
    password: str
    is_required: str = Field(alias="is-required")

class OdlReq(BaseModel):
  data_provider_input: DataProviderInputObj = Field(alias="data-provider:input")

'''
    Request send to ONOS
    {
  "devices": {
    "netconf:192.168.0.107:18310": {
      "netconf": {
        "ip": "192.168.0.107",
        "port": 18310,
        "username": "netconf",
        "password": "netconf!"
      },
      "basic": {
        "driver": "ovs-netconf"
      }
    }
  }
}
'''

def SendONOSReq(ip, port, username, password, onosIp):
  hostInfoBody = {}
  hostInfoBody["ip"] = ip
  hostInfoBody["port"] = int(port)
  hostInfoBody["username"] = username
  hostInfoBody["password"] = password

  driverBody = {}
  driverBody["driver"] = "ovs-netconf"

  netconfBody = {}
  netconfBody["netconf"] = hostInfoBody
  netconfBody["basic"] = driverBody

  deviceBody = {}
  deviceBody["netconf:"+ip+":"+port] = netconfBody

  payload = {}
  payload["devices"] = deviceBody
  

  url = "http://" + onosIp + ":8181/onos/v1/network/configuration"
  response = requests.post(url, auth=HTTPBasicAuth('onos', 'rocks'), json = payload, headers={'content-type': 'application/json'})
  return response.status_code
  #pass
'''
    Simple ODL response in JSON:
    {"data-provider:output":{"id":"o-ru","is-required":true,"host":"10.0.2.15","port":18300,"password":"netconf!","node-id":"o-ru","username":"netconf"}}
'''

def BuildOdlResponse(id, nodeId, host, port, username, password, isRequired):
  providerBody = {}
  providerBody["id"] = id
  providerBody["is-required"] = isRequired
  providerBody["host"] = host
  providerBody["port"] = port
  providerBody["password"] = password
  providerBody["node-id"] = nodeId
  providerBody["username"] = username

  response = {}
  response["data-provider:output"] = providerBody
  return response
