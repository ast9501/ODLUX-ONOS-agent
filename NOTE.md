# Dev note

## FastAPI
### [Process field name that have non-alphanumeric characters](https://stackoverflow.com/questions/66185920/pydantic-model-with-field-names-that-have-non-alphanumeric-characters)

### [Handle xml request](https://github.com/tiangolo/fastapi/issues/558)

## ODL api doc
### [Mount new network element](https://docs.opendaylight.org/projects/netconf/en/latest/user-guide.html#spawning-new-netconf-connectors)
* PUT method for rfc8040