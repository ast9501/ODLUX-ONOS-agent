FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-slim

WORKDIR /src
COPY requirements.txt .
COPY main.py .
COPY package ./package
COPY config ./config
RUN pip3 install -r requirements.txt

CMD ["uvicorn", "main:app", "--reload", "--host", "0.0.0.0", "--port", "8000"]

EXPOSE 8000